import React, { Suspense, lazy } from "react";
import { HashRouter, Route, Switch } from "react-router-dom";

import Loader from "../util/Loader/Loader";
const LoginPage = lazy(() => import("../Components/LoginPage/LoginPage"));
const SignUp = lazy(() => import("../Components/SignUp/SignUp"));
const UserList = lazy(() => import("../Components/UserList/UserList"));
// const FormStep = lazy(() => import("../Components/FormStep/FormStep"));
const UserDetails = lazy(() => import("../Components/UserDetails/UserDetails"));
const UserInfo = lazy(() => import("../Components/UserInfo/UserInfo"));
const UserDoc = lazy(() => import("../Components/UserDoc/UserDoc"));

const NotFound = lazy(() => import("../util/NotFound/NotFound"));

export const Routes = () => {
  return (
    <HashRouter>
      <Suspense fallback={<Loader />}>
        <Switch>
          <Route exact path="/signIn" component={LoginPage} />
          <Route exact path="/signUp" component={SignUp} />
          <Route exact path="/userList" component={UserList} />
          {/* <Route exact path="/formStep" component={FormStep} /> */}
          <Route exact path="/userDetails" component={UserDetails} />
          <Route exact path="/userInfo" component={UserInfo} />
          <Route exact path="/userDoc" component={UserDoc} />

          <Route exact path="*" component={NotFound} />
        </Switch>
      </Suspense>
    </HashRouter>
  );
};
