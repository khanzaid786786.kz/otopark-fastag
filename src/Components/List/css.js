import { makeStyles, fade } from "@material-ui/core/styles";
export const drawerWidth = 240;

export const useStyles = makeStyles(theme => ({
  root: {
    width: "100%"
  },
  container: {
    maxHeight: "100vh",
    height: "100vh"
  },
  addIconDiv: {
    position: "fixed",
    bottom: "2%",
    right: "5%",
    backgroundColor: "#00BBDC",
    "&:hover": {
      background: "#fff",
      color: "#00BBDC",
      border: "2px solid #00BBDC",
      borderRadius: "50%"
    }
  },
  addIcon: {
    width: "30px",
    height: "30px",
    color: "#fff",
    "&:hover": {
      color: "#00BBDC"
    }
  }
}));
