import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import IconButton from "@material-ui/core/IconButton";
import AddIcon from "@material-ui/icons/Add";
import { Link } from "react-router-dom";
import Appbar from "../Appbar/Appbar";
import { useStyles } from "./css";

const columns = [
  { id: "name", label: "Name", minWidth: 170, align: "center" },
  { id: "number", label: "Contact Number", minWidth: 100, align: "center" },
  { id: "tagId", label: "Tag ID", minWidth: 100, align: "center" },
  { id: "recharge", label: "Recharge", minWidth: 100, align: "center" }
];

function createData(name, number, tagId, recharge) {
  return { name, number, tagId, recharge };
}

const rows = [
  createData("Zaid Khan 1", 1324171354, 123287263, 500),
  createData("Zaid Khan 2", 1324171354, 123287263, 500),
  createData("Zaid Khan 3", 1324171354, 123287263, 500),
  createData("Zaid Khan 4", 1324171354, 123287263, 500),
  createData("Zaid Khan 5", 1324171354, 123287263, 500),
  createData("Zaid Khan 6", 1324171354, 123287263, 500),
  createData("Zaid Khan 7", 1324171354, 123287263, 500),
  createData("Zaid Khan 8", 1324171354, 123287263, 500),
  createData("Zaid Khan 9", 1324171354, 123287263, 500),
  createData("Zaid Khan 10", 1324171354, 123287263, 500),
  createData("Zaid Khan 11", 1324171354, 123287263, 500),
  createData("Zaid Khan 12", 1324171354, 123287263, 500),
  createData("Zaid Khan 13", 1324171354, 123287263, 500),
  createData("Zaid Khan 14", 1324171354, 123287263, 500),
  createData("Zaid Khan 15", 1324171354, 123287263, 500)
];

export default function StickyHeadTable() {
  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(15);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <>
      <Appbar />
      <h1 style={{ color: "#00BBDC", marginTop: "100px", textAlign: "center" }}>
        List of all Users
      </h1>
      <Paper className={classes.root}>
        <TableContainer className={classes.container}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map(column => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{
                      minWidth: column.minWidth,
                      backgroundColor: "#00BBDC",
                      color: "#fff"
                    }}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {rows
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map(row => {
                  return (
                    <TableRow
                      hover
                      role="checkbox"
                      tabIndex={-1}
                      key={row.code}
                    >
                      {columns.map(column => {
                        const value = row[column.id];
                        return (
                          <TableCell key={column.id} align={column.align}>
                            {column.format && typeof value === "number"
                              ? column.format(value)
                              : value}
                          </TableCell>
                        );
                      })}
                    </TableRow>
                  );
                })}
            </TableBody>
          </Table>
        </TableContainer>
        {/* <TablePagination
          rowsPerPageOptions={[10, 25, 100]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        /> */}
        <Link to="/userDetails">
          <IconButton className={classes.addIconDiv}>
            <AddIcon className={classes.addIcon} />
          </IconButton>
        </Link>
      </Paper>
    </>
  );
}
