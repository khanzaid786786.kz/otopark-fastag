import React, { useEffect } from "react";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Checkbox from "@material-ui/core/Checkbox";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import RefreshIcon from "@material-ui/icons/Refresh";
import { FilePicker } from "react-file-picker";
import { Link } from "react-router-dom";
import ProfileImage from "../ProfileImage/ProfileImage";
import { useStyles } from "./css";

export default function FullWidthGrid() {
  const classes = useStyles();
  const [checked, setChecked] = React.useState(true);
  const [showField, setShowField] = React.useState("");

  const handleChange = event => {
    setChecked(event.target.checked);
  };

  const openRequest = () => {
    setShowField(true);
  };

  return (
    <div className={classes.root}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <form className={classes.formRoot} noValidate autoComplete="off">
            <h1 style={{ textAlign: "center", color: "#00BBDC" }}>
              Sign Up Form
            </h1>
            <ProfileImage />

            <label className={classes.label}>
              First Name <sup style={{ color: "red" }}>*</sup>
            </label>
            <TextField
              size="small"
              className={classes.inputField}
              fullWidth
              variant="outlined"
            />
            <label className={classes.label}>
              Last Name<sup style={{ color: "red" }}>*</sup>
            </label>
            <TextField
              size="small"
              className={classes.inputField}
              fullWidth
              variant="outlined"
            />
            <label className={classes.label}>
              Email Address<sup style={{ color: "red" }}>*</sup>
            </label>
            <TextField
              size="small"
              className={classes.inputField}
              fullWidth
              variant="outlined"
            />
            <label className={classes.label}>
              Address<sup style={{ color: "red" }}>*</sup>
            </label>
            <TextField
              size="small"
              className={classes.inputField}
              fullWidth
              variant="outlined"
            />
            <label className={classes.label}>
              Contact Number<sup style={{ color: "red" }}>*</sup>
            </label>
            <TextField
              type="number"
              size="small"
              className={classes.inputField}
              fullWidth
              variant="outlined"
            />
            <div style={{ marginBottom: "20px" }}>
              <label className={classes.label}>
                Document Type<sup style={{ color: "red" }}>*</sup>
              </label>
              <select className={classes.dropdown}>
                <option>Permanent X</option>
                <option>Permanent y</option>
                <option>Permanent Z</option>
                <option>Permanent X</option>
              </select>
            </div>
            <div className={classes.flexDiv}>
              <TextField
                size="small"
                className={classes.inputField}
                fullWidth
                variant="outlined"
              />
              <FilePicker extensions={["md"]}>
                <button className={classes.chooseButton}>Add Documnet</button>
              </FilePicker>
            </div>
            <label className={classes.label}>
              Password<sup style={{ color: "red" }}>*</sup>
            </label>
            <TextField
              type="password"
              size="small"
              className={classes.inputField}
              fullWidth
              variant="outlined"
            />
            <label className={classes.label}>
              Confirm Password<sup style={{ color: "red" }}>*</sup>
            </label>
            <TextField
              type="password"
              size="small"
              className={classes.inputField}
              fullWidth
              variant="outlined"
            />

            <div className={classes.flexDiv}>
              <Checkbox
                style={{ color: "#00BBDC" }}
                // checked={checked}
                onChange={handleChange}
                inputProps={{ "aria-label": "primary checkbox" }}
              />
              <p className={classes.checkboxLabel}>
                I have accept your{" "}
                <b style={{ color: "#00BBDC" }}>term & conditions</b>
              </p>
            </div>
            <Link to="/userList" style={{ textDecoration: "none" }}>
              <Button className={classes.nextButton}>Sign Up</Button>
            </Link>
          </form>
        </Grid>
      </Grid>
    </div>
  );
}
