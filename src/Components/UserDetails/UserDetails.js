import React from "react";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Appbar from "../Appbar/Appbar";
import Stepper from "../Stepper/Stepper";
import TextField from "@material-ui/core/TextField";
import Checkbox from "@material-ui/core/Checkbox";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import RefreshIcon from "@material-ui/icons/Refresh";
import { Link } from "react-router-dom";
import { useStyles } from "./css";

export default function FullWidthGrid(props) {
  const classes = useStyles();
  const [checked, setChecked] = React.useState(true);
  const [showField, setShowField] = React.useState("");

  const handleChange = event => {
    setChecked(event.target.checked);
  };

  const openRequest = () => {
    setShowField(true);
  };

  return (
    <div className={classes.root}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Appbar />
          <Stepper />
          {/* Stepper */}
          {/* <Grid container spacing={2}>
            <Grid item xs={4} align="center">
              <p className={classes.numberTitle}>User Details</p>
              <div className={classes.numberDiv}>1</div>
            </Grid>
            <Grid item xs={4} align="center">
              <p className={classes.numberTitle1}>User Information</p>
              <div className={classes.numberDiv1}>2</div>
            </Grid>
            <Grid item xs={4} align="center">
              <p className={classes.numberTitle1}>User Documents</p>
              <div className={classes.numberDiv1}>3</div>
            </Grid>
          </Grid>
          <div
            style={{ borderBottom: "2px solid red", marginTop: "-25px", content:"none" }}
          ></div> */}

          {/* Stepper */}

          <form className={classes.formRoot} noValidate autoComplete="off">
            <FormControl component="fieldset">
              <h4>Are You an existing IDFC First Bank Customer?</h4>
              <RadioGroup
                row
                aria-label="position"
                name="position"
                defaultValue="top"
              >
                <FormControlLabel
                  value="start"
                  control={
                    <Radio style={{ color: "#00BBDC" }} color="primary" />
                  }
                  label="Yes"
                  labelPlacement="start"
                />
                <FormControlLabel
                  value="start1"
                  control={
                    <Radio style={{ color: "#00BBDC" }} color="primary" />
                  }
                  label="No"
                  labelPlacement="start"
                />
              </RadioGroup>
            </FormControl>
            <br />
            <br />
            <label className={classes.label}>
              New Vehicle / Chassis No <sup style={{ color: "red" }}>*</sup>
            </label>
            <TextField
              size="small"
              className={classes.inputField}
              fullWidth
              variant="outlined"
            />
            <label className={classes.label}>
              UCIC / Customer ID <sup style={{ color: "red" }}>*</sup>
            </label>
            <TextField
              size="small"
              className={classes.inputField}
              fullWidth
              variant="outlined"
            />
            <p>
              <sup>*</sup>
              OTP will sent to registered mobile number <br />
              self-tag issuance is only for CAR/JEEP/VAN vehicles.
            </p>
            <div className={classes.flexDiv}>
              <Checkbox
                style={{ color: "#00BBDC" }}
                // checked={checked}
                onChange={handleChange}
                inputProps={{ "aria-label": "primary checkbox" }}
              />
              <p className={classes.checkboxLabel}>User Consent </p>
            </div>

            <div className={classes.flexDiv}>
              <div className={classes.numberBox}>
                <h2>5535</h2>
              </div>
              <RefreshIcon className={classes.refreshIcon} />
            </div>

            <TextField
              size="small"
              className={classes.inputField}
              fullWidth
              variant="outlined"
              placeholder="Please enter the number"
            />

            {!showField && (
              <div>
                <Button onClick={openRequest} className={classes.verifyButton}>
                  Request to Verify
                </Button>
              </div>
            )}

            {showField && (
              <div>
                <label className={classes.label}>Enter OTP</label>
                <TextField
                  size="small"
                  className={classes.inputField}
                  fullWidth
                  variant="outlined"
                />
                <Link to="/userInfo" style={{ textDecoration: "none" }}>
                  <Button className={classes.verifyButton}>Verify</Button>
                </Link>
              </div>
            )}

            {/* <Button className={classes.verifyButton}>Request to Verify</Button> */}
          </form>
        </Grid>
      </Grid>
    </div>
  );
}
