import { makeStyles, fade } from "@material-ui/core/styles";
export const drawerWidth = 240;

export const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    "& label.Mui-focused": {
      color: "#00BBDC"
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "#00BBDC"
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderColor: "lighgray"
      },
      "&:hover fieldset": {
        borderColor: "lightgray"
      },
      "&.Mui-focused fieldset": {
        borderColor: "#00BBDC"
      }
    }
  },
  formRoot: {
    marginLeft: "20%",
    marginRight: "20%",
    [theme.breakpoints.down("sm")]: {
      marginLeft: "5%",
      marginRight: "5%"
    }
  },
  numberTitle: {
    fontWeight: "bold",
    fontSize: "12px"
  },
  numberTitle1: {
    color: "lightgrey",
    fontSize: "12px"
  },
  numberDiv: {
    fontSize: "25px",
    width: "30px",
    height: "30px",
    borderRadius: "50%",
    textAlign: "center",
    backgroundColor: "#00BBDC",
    color: "#fff"
  },
  numberDiv1: {
    fontSize: "25px",
    width: "30px",
    height: "30px",
    borderRadius: "50%",
    textAlign: "center",
    backgroundColor: "#fff",
    color: "#00BBDC",
    border: "1px solid #00BBDC"
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary
  },
  label: {
    fontWeight: "bold"
  },
  inputField: {
    marginBottom: "20px",
    backgroundColor: "#F7F7F7"
  },
  para: {
    color: "red"
  },
  flexDiv: {
    display: "flex"
  },
  checkboxLabel: {
    color: "#00BBDC"
  },
  numberBox: {
    marginBottom: "20px",
    textAlign: "center",
    width: "150px",
    borderRadius: "10px",
    border: "1px solid black"
  },
  refreshIcon: {
    marginLeft: "10px",
    color: "#fff",
    backgroundColor: "#00BBDC",
    borderRadius: "50%",
    padding: "5px"
  },
  verifyButton: {
    textTransform: "none",
    fontSize: "20px",
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    color: "#fff",
    backgroundColor: "#00BBDC",
    borderRadius: "20px",
    width: "60%",
    "&:hover": {
      background: "#fff",
      color: "#00BBDC",
      border: "2px solid #00BBDC"
    }
  }
}));
