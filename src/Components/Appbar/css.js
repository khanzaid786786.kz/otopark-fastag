import { makeStyles, fade } from "@material-ui/core/styles";
export const drawerWidth = 240;

export const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: "-8px",
    marginLeft: "-8px",
    marginRight: "-8px"
  },
  appContainer: {
    backgroundColor: "#00BBDC"
  },
  logoImage: {
    width: "70px",
    height: "50px"
  },
  name: {
    fontSize: "20px",
    fontWeight: "bold",
    [theme.breakpoints.down("sm")]: {
      fontSize: "12px"
    }
  },
  nameId: {
    fontSize: "12px",
    fontWeight: "bold",
    marginTop: "-15px",
    [theme.breakpoints.down("sm")]: {
      marginTop: "-8px",
      fontSize: "10px"
    }
  },
  title: {
    flexGrow: 1,
    marginLeft: "20px"
  },
  logoutButton: {
    color: "#fff",
    textTransform: "none",
    [theme.breakpoints.down("sm")]: {
      display: "None"
    }
  },
  logoutIcon: {
    [theme.breakpoints.down("sm")]: {
      display: "None"
    }
  },
  mobileAppbar: {
    backgroundColor: "#00BBDC",
    color: "black",
    [theme.breakpoints.down("sm")]: {
      width: "100%",
      position: "fixed"
    }
  }
}));
