import React from "react";
import ReactDOM from "react-dom";
import ImageUploading from "react-images-uploading";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import IconButton from "@material-ui/core/IconButton";
import EditIcon from "@material-ui/icons/Edit";
import { useStyles } from "./css";

export default function FullWidthGrid() {
  const classes = useStyles();
  const [show, setShow] = React.useState("");
  const [images, setImages] = React.useState([]);
  const maxNumber = 1;
  const onChange = (imageList, addUpdateIndex) => {
    // data for submit
    console.log(imageList, addUpdateIndex);
    setImages(imageList);
    setShow(true);
  };

  const showProfile = () => {
    setShow(true);
  };

  return (
    <div className="App">
      <ImageUploading
        multiple
        value={images}
        onChange={onChange}
        maxNumber={maxNumber}
        dataURLKey="data_url"
      >
        {({
          imageList,
          onImageUpload,
          onImageRemoveAll,
          onImageUpdate,
          onImageRemove,
          isDragging,
          dragProps
        }) => (
          // write your building UI
          <div>
            {!show && (
              <>
                <div className="upload__image-wrapper">
                  <IconButton
                    className={classes.selectIconDiv}
                    style={isDragging ? { color: "red" } : null}
                    onClick={onImageUpload}
                    {...dragProps}
                  >
                    <AccountCircleIcon className={classes.selectIcon} />
                  </IconButton>
                  &nbsp;
                </div>
              </>
            )}

            {show && (
              <>
                {imageList.map((image, index) => (
                  <div key={index} className="image-item">
                    <img
                      className={classes.profileImage}
                      src={image.data_url}
                      alt=""
                      width="100"
                    />
                    <div className={classes.maineditButtonDiv}>
                      <IconButton
                        className={classes.editButtonDiv}
                        onClick={() => onImageUpdate(index)}
                      >
                        <EditIcon className={classes.editButton} />
                      </IconButton>
                    </div>
                  </div>
                ))}
              </>
            )}
          </div>
        )}
      </ImageUploading>
    </div>
  );
}
