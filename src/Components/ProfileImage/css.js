import { makeStyles, fade } from "@material-ui/core/styles";
export const drawerWidth = 240;

export const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  selectIconDiv: {
    display: "block",
    marginLeft: "auto",
    marginRight: "auto"
  },
  selectIcon: {
    color: "#00BBDC",
    width: "150px",
    height: "150px"
  },
  profileImage: {
    width: "150px",
    height: "150px",
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    borderRadius: "50%"
  },
  maineditButtonDiv: {
    position: "relative"
  },
  editButtonDiv: {
    position: "absolute",
    bottom: 0,
    right: "15%"
  },
  editButton: {
    width: "30px",
    height: "30px",
    color: "#00BBDC"
  }
}));
