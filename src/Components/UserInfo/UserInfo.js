import React, { useEffect } from "react";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Appbar from "../Appbar/Appbar";
import Stepper from "../Stepper/Stepper";
import TextField from "@material-ui/core/TextField";
import Checkbox from "@material-ui/core/Checkbox";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import RefreshIcon from "@material-ui/icons/Refresh";
import { FilePicker } from "react-file-picker";
import { Link } from "react-router-dom";
import { useStyles } from "./css";

export default function FullWidthGrid() {
  const classes = useStyles();
  const [checked, setChecked] = React.useState(true);
  const [showField, setShowField] = React.useState("");

  const handleChange = event => {
    setChecked(event.target.checked);
  };

  const openRequest = () => {
    setShowField(true);
  };

  return (
    <div className={classes.root}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Appbar />
          <Stepper />
          <form className={classes.formRoot} noValidate autoComplete="off">
            <div className={classes.flexDiv}>
              <p className={classes.checkboxLabel}>KYC </p>
              <Checkbox
                style={{ color: "#00BBDC" }}
                // checked={checked}
                onChange={handleChange}
                inputProps={{ "aria-label": "primary checkbox" }}
              />
              <p style={{ fontSize: "13px" }}>
                (Customer Details mandatory for KYC)
              </p>
            </div>
            <label className={classes.label}>
              Name / Company <sup style={{ color: "red" }}>*</sup>
            </label>
            <TextField
              size="small"
              className={classes.inputField}
              fullWidth
              variant="outlined"
            />
            <label className={classes.label}>
              DOB<sup style={{ color: "red" }}>*</sup>
            </label>
            <TextField
              size="small"
              className={classes.inputField}
              fullWidth
              variant="outlined"
            />
            <label className={classes.label}>
              Local Address<sup style={{ color: "red" }}>*</sup>
            </label>
            <TextField
              size="small"
              className={classes.inputField}
              fullWidth
              variant="outlined"
            />
            <div style={{ marginBottom: "20px" }}>
              <label className={classes.label}>
                State<sup style={{ color: "red" }}>*</sup>
              </label>
              <select className={classes.dropdown}>
                <option>Maharashtra</option>
                <option>Delhi</option>
                <option>Punjab</option>
                <option>Aasam</option>
              </select>
            </div>
            <label className={classes.label}>
              PIN<sup style={{ color: "red" }}>*</sup>
            </label>
            <TextField
              size="small"
              className={classes.inputField}
              fullWidth
              variant="outlined"
            />
            <label className={classes.label}>
              Email<sup style={{ color: "red" }}>*</sup>
            </label>
            <TextField
              size="small"
              className={classes.inputField}
              fullWidth
              variant="outlined"
            />
            <div style={{ marginBottom: "20px" }}>
              <label className={classes.label}>
                Document Type<sup style={{ color: "red" }}>*</sup>
              </label>
              <select className={classes.dropdown}>
                <option>Permanent X</option>
                <option>Permanent y</option>
                <option>Permanent Z</option>
                <option>Permanent X</option>
              </select>
            </div>
            <label className={classes.label}>
              Document Number<sup style={{ color: "red" }}>*</sup>
            </label>
            <div className={classes.flexDiv}>
              <TextField
                size="small"
                className={classes.inputField}
                fullWidth
                variant="outlined"
              />
              <FilePicker
                extensions={["md"]}
                // onChange={FileObject => ()}
                // onError={errMsg => ()}
              >
                <button className={classes.chooseButton}>Choose file</button>
              </FilePicker>
            </div>
            <div className={classes.flexDiv}>
              <Checkbox
                style={{ color: "#00BBDC" }}
                onChange={handleChange}
                inputProps={{ "aria-label": "primary checkbox" }}
              />
              <p className={classes.checkboxLabel}>Commercial</p>
            </div>
            <div
              style={{
                borderBottom: "1px solid black",
                marginLeft: "-7%",
                marginRight: "-7%",
                marginTop: "30px",
                marginBottom: "30px"
              }}
            ></div>
            <label className={classes.label}>
              Barcode<sup style={{ color: "red" }}>*</sup>
            </label>
            <div className={classes.flexDiv}>
              <TextField
                size="small"
                className={classes.inputField}
                fullWidth
                variant="outlined"
              />
              <FilePicker extensions={["md"]}>
                <button className={classes.chooseButton}>Assign</button>
              </FilePicker>
            </div>
            <div style={{ marginBottom: "20px" }}>
              <label className={classes.label}>
                CCH Class<sup style={{ color: "red" }}>*</sup>
              </label>
              <select className={classes.dropdown}>
                <option>Car / Jeep</option>
                <option>Car / Jeep</option>
                <option>Car / Jeep</option>
                <option>Car / Jeep</option>
              </select>
            </div>
            <div style={{ marginBottom: "20px" }}>
              <label className={classes.label}>
                Product<sup style={{ color: "red" }}>*</sup>
              </label>
              <select className={classes.dropdown}>
                <option>Product 1</option>
                <option>Product 2</option>
                <option>Product 3</option>
                <option>Product 4</option>
              </select>
            </div>
            <label className={classes.label}>
              Initial Deposit(&#8377;)<sup style={{ color: "red" }}>*</sup>
            </label>
            <TextField
              size="small"
              className={classes.inputField}
              fullWidth
              variant="outlined"
            />
            <p>225.00 (&#8377;)</p>
            <div className={classes.rateDiv}>
              <p style={{ fontWeight: "bold" }}>
                <sup>*</sup>Issurance Amount has to be greater than sum of
                issurance fee, Security Amount & Minimum Balance
              </p>
              <p style={{ color: "#fff" }}>
                Issurance fee 50.00, Security Amount 75.00 <br />
                Minimum Balance 100.00(&#8377;)
              </p>
            </div>
            <div className={classes.flexDiv}>
              <Checkbox
                style={{ color: "#00BBDC" }}
                // checked={checked}
                onChange={handleChange}
                inputProps={{ "aria-label": "primary checkbox" }}
              />
              <p className={classes.checkboxLabel}>
                Bt clicking, you accept our{" "}
                <b style={{ color: "#00BBDC" }}>term & conditions</b>
              </p>
            </div>
            <Link to="/userDoc" style={{ textDecoration: "none" }}>
              <Button className={classes.nextButton}>Next</Button>
            </Link>
          </form>
        </Grid>
      </Grid>
    </div>
  );
}
