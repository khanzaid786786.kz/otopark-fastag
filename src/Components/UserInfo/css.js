import { makeStyles, fade } from "@material-ui/core/styles";
export const drawerWidth = 240;

export const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    "& label.Mui-focused": {
      color: "#00BBDC"
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "#00BBDC"
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderColor: "lighgray"
      },
      "&:hover fieldset": {
        borderColor: "lightgray"
      },
      "&.Mui-focused fieldset": {
        borderColor: "#00BBDC"
      }
    }
  },
  formRoot: {
    marginLeft: "20%",
    marginRight: "20%",
    [theme.breakpoints.down("sm")]: {
      marginLeft: "5%",
      marginRight: "5%"
    }
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary
  },
  label: {
    fontWeight: "bold"
  },
  inputField: {
    marginBottom: "20px",
    backgroundColor: "#F7F7F7"
  },
  para: {
    color: "red"
  },
  flexDiv: {
    display: "flex"
  },
  dropdown: {
    fontSize: "15px",
    backgroundColor: "#F7F7F7",
    marginLeft: "1px",
    width: "100%",
    height: "45px",
    "&:focus": {
      borderColor: "#00BBDC"
    }
  },
  checkboxLabel: {
    fontSize: "14px",
    color: "black"
  },
  chooseButton: {
    borderRadius: "5px",
    marginLeft: "20px",
    width: "100px",
    height: "40px",
    color: "#fff",
    border: "1px solid #00BBDC",
    backgroundColor: "#00BBDC"
  },
  rateDiv: {
    marginLeft: "-7%",
    marginRight: "-7%",
    textAlign: "center",
    padding: "10px",
    backgroundColor: "#00BBDC"
  },
  nextButton: {
    textTransform: "none",
    fontSize: "20px",
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    color: "#fff",
    backgroundColor: "#00BBDC",
    borderRadius: "20px",
    width: "60%",
    "&:hover": {
      background: "#fff",
      color: "#00BBDC",
      border: "2px solid #00BBDC"
    }
  }
}));
