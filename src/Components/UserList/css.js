import { makeStyles, fade } from "@material-ui/core/styles";

export const useStyles = makeStyles(theme => ({
  appRoot: {
    flexGrow: 1,
    marginTop: "-8px",
    marginLeft: "-8px",
    marginRight: "-8px"
  },
  appContainer: {
    backgroundColor: "#00BBDC"
  },
  logoImage: {
    width: "70px",
    height: "50px"
  },
  root: {
    width: "100%",
    marginTop: "20px"
  },
  appbar: {
    marginLeft: "-8px",
    marginRight: "-8px"
  },
  report: {
    marginLeft: "10%",
    marginRight: "10%",
    // overflow: "auto",
    height: "100vh",
    [theme.breakpoints.down("sm")]: {
      marginLeft: "0%",
      marginRight: "0%",
      height: "100vh"
    }
  },
  moreButton: {
    textTransform: "none",
    color: "#fff",
    backgroundColor: "#00BBDC",
    "&:hover": {
      color: "#00BBDC",
      backgroundColor: "white",
      border: "1px solid #00BBDC"
    }
  },
  search: {
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    "&:hover": {
      backgroundColor: fade(theme.palette.common.white, 0.25)
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(3),
      width: "100%"
    }
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  inputRoot: {
    color: "inherit"
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("md")]: {
      width: "20ch"
    }
  },
  content: {
    fontSize: "15px"
  },
  viewMoreBtn: {
    marginTop: "50px",
    textTransform: "none",
    width: "40%",
    color: "white",
    backgroundColor: "#00BBDC",
    borderRadius: "20px",
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    "&:hover": {
      color: "#00BBDC",
      backgroundColor: "white",
      border: "1px solid #00BBDC"
    }
  },
  addIconDiv: {
    position: "fixed",
    bottom: "2%",
    right: "5%",
    backgroundColor: "#00BBDC",
    "&:hover": {
      background: "#fff",
      color: "#00BBDC",
      border: "2px solid #00BBDC",
      borderRadius: "50%"
    }
  },
  addIcon: {
    width: "30px",
    height: "30px",
    color: "#fff",
    "&:hover": {
      color: "#00BBDC"
    }
  },
  desktopView: {
    display: "block",
    [theme.breakpoints.down("sm")]: {
      display: "block"
      // marginBottom: "20px"
    }
  }
}));
