import React from "react";
import PropTypes from "prop-types";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import useScrollTrigger from "@material-ui/core/useScrollTrigger";
import { Card, CardContent, Grid, InputBase } from "@material-ui/core";
import Box from "@material-ui/core/Box";
import Container from "@material-ui/core/Container";
import Fab from "@material-ui/core/Fab";
import KeyboardArrowUpIcon from "@material-ui/icons/KeyboardArrowUp";
import Zoom from "@material-ui/core/Zoom";
import Button from "@material-ui/core/Button";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import SearchIcon from "@material-ui/icons/Search";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import AddIcon from "@material-ui/icons/Add";
import { Link } from "react-router-dom";
import { useStyles } from "./css";

function ScrollTop(props) {
  const { children, window } = props;
  const classes = useStyles();
  // Note that you normally won't need to set the window ref as useScrollTrigger
  // will default to window.
  // This is only being set here because the demo is in an iframe.
  const trigger = useScrollTrigger({
    target: window ? window() : undefined,
    disableHysteresis: true,
    threshold: -1
  });

  const handleClick = event => {
    const anchor = (event.target.ownerDocument || document).querySelector(
      "#back-to-top-anchor"
    );

    if (anchor) {
      anchor.scrollIntoView({ behavior: "smooth", block: "center" });
    }
  };

  return (
    <Zoom in={trigger}>
      <div onClick={handleClick} role="presentation" className={classes.root}>
        {children}
      </div>
    </Zoom>
  );
}

ScrollTop.propTypes = {
  children: PropTypes.element.isRequired,
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window: PropTypes.func
};

export default function BackToTop(props) {
  const classes = useStyles();

  return (
    <div className={classes.report}>
      <React.Fragment>
        <div className={classes.appRoot}>
          <AppBar position="fixed" className={classes.appContainer}>
            <Toolbar>
              <IconButton
                edge="start"
                className={classes.menuButton}
                color="inherit"
                aria-label="menu"
              >
                <img
                  className={classes.logoImage}
                  src={require("../../assets/images/whiteLogo.png")}
                />
              </IconButton>
              <Typography variant="h6" className={classes.title}>
                {/* News */}
              </Typography>
              <div className={classes.search}>
                <div className={classes.searchIcon}>
                  <SearchIcon />
                </div>
                <InputBase
                  placeholder="Search…"
                  classes={{
                    root: classes.inputRoot,
                    input: classes.inputInput
                  }}
                  inputProps={{ "aria-label": "search" }}
                />
              </div>
              <Button color="inherit">
                EXIT <ExitToAppIcon />
              </Button>
            </Toolbar>
          </AppBar>
        </div>
        <Toolbar id="back-to-top-anchor" />
        <Container>
          <Box my={2}>
            <Grid item xs={12}>
              <Grid
                container
                spacing={2}
                direction="row"
                justify="center"
                alignItems="center"
              >
                {[...new Array(12)].map(() => {
                  return (
                    <>
                      <Card className={classes.root}>
                        <CardContent>
                          <Grid container spacing={2}>
                            {/* <Grid item lg={4}>
                              <div style={{ marginTop: "40px" }}>
                                <img
                                  src={require("../../assets/images/profile.png")}
                                  width="70px"
                                  height="70px"
                                />
                                <p style={{ fontSize: "15px" }}>
                                  Booking_Id: 123456
                                </p>
                              </div>
                            </Grid> */}

                            <Grid item lg={12}>
                              <div className={classes.content}>
                                <p>
                                  <b>Name</b>: 123456
                                </p>
                                <p>
                                  <b>Contact Number</b>: 1234567890
                                </p>
                                <p>
                                  <b>Tag ID</b>: 223366
                                </p>
                                <p>
                                  <b>Recharge</b>: 500
                                </p>
                                <Button className={classes.moreButton}>
                                  More Details
                                </Button>
                              </div>
                            </Grid>
                          </Grid>
                        </CardContent>
                      </Card>
                    </>
                  );
                })}
              </Grid>
            </Grid>
          </Box>
          <Button className={classes.viewMoreBtn}>View More</Button>
        </Container>
        <ScrollTop {...props}>
          <Fab
            color="secondary"
            size="small"
            aria-label="scroll back to top"
            style={{ backgroundColor: "#00BBDC", marginTop: "-60px" }}
          >
            <KeyboardArrowUpIcon style={{ color: "#fff" }} />
          </Fab>
        </ScrollTop>
        <Link to="/userDetails">
          <IconButton className={classes.addIconDiv}>
            <AddIcon className={classes.addIcon} />
          </IconButton>
        </Link>
      </React.Fragment>
    </div>
  );
}
