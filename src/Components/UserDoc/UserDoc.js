import React from "react";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Appbar from "../Appbar/Appbar";
import Stepper from "../Stepper/Stepper";
import TextField from "@material-ui/core/TextField";
import DoneIcon from "@material-ui/icons/Done";
import { Link } from "react-router-dom";
import { FilePicker } from "react-file-picker";
import { useStyles } from "./css";

export default function FullWidthGrid() {
  const classes = useStyles();
  const [checked, setChecked] = React.useState(true);
  const [showField, setShowField] = React.useState("");

  const handleChange = event => {
    setChecked(event.target.checked);
  };

  const openRequest = () => {
    setShowField(true);
  };

  return (
    <div className={classes.root}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Appbar />
          <Stepper />

          <form className={classes.formRoot} noValidate autoComplete="off">
            {!showField && (
              <>
                <label className={classes.label}>
                  vehicle Image<sup style={{ color: "red" }}>*</sup>
                </label>
                <div className={classes.flexDiv}>
                  <FilePicker extensions={["md"]}>
                    <button className={classes.chooseButton}>
                      Choose file
                    </button>
                  </FilePicker>
                  <div>
                    <p>file name</p>
                  </div>
                </div>

                <label className={classes.label}>
                  Rc Number<sup style={{ color: "red" }}>*</sup>
                </label>
                <div className={classes.flexDiv}>
                  <FilePicker extensions={["md"]}>
                    <button className={classes.chooseButton}>
                      Choose file
                    </button>
                  </FilePicker>
                  <div>
                    <p>file name</p>
                  </div>
                </div>
                <Grid container spacing={2} style={{ marginTop: "50px" }}>
                  <Grid item xs={6}>
                    <p className={classes.logoTitle}>Powered by : </p>
                    <img
                      className={classes.logoImg}
                      src={require("../../assets/images/fastagImg.jpg")}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <p className={classes.logoTitle}>Supported by : </p>
                    <img
                      className={classes.logoImg}
                      src={require("../../assets/images/idfcBank.jpg")}
                    />
                  </Grid>
                </Grid>
                <Button onClick={openRequest} className={classes.issueButton}>
                  Issue & Activate
                </Button>
              </>
            )}

            {showField && (
              <>
                <div>
                  <DoneIcon className={classes.doneIcon} />
                  <h1 className={classes.doneIcontitle}>Thank You!</h1>
                  <p className={classes.doneIconPara}>
                    You request will be placed in 2-3 days
                  </p>
                </div>
                <Grid container spacing={2} style={{ marginTop: "50px" }}>
                  <Grid item xs={6}>
                    <p className={classes.logoTitle}>Powered by : </p>
                    <img
                      className={classes.logoImg}
                      src={require("../../assets/images/fastagImg.jpg")}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <p className={classes.logoTitle}>Supported by : </p>
                    <img
                      className={classes.logoImg}
                      src={require("../../assets/images/idfcBank.jpg")}
                    />
                  </Grid>
                </Grid>
                <Button onClick={openRequest} className={classes.issueButton}>
                  Done
                </Button>
              </>
            )}
          </form>
        </Grid>
      </Grid>
    </div>
  );
}
