import { makeStyles, fade } from "@material-ui/core/styles";
export const drawerWidth = 240;

export const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    "& label.Mui-focused": {
      color: "#00BBDC"
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "#00BBDC"
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderColor: "lighgray"
      },
      "&:hover fieldset": {
        borderColor: "lightgray"
      },
      "&.Mui-focused fieldset": {
        borderColor: "#00BBDC"
      }
    }
  },
  formRoot: {
    marginLeft: "20%",
    marginRight: "20%",
    [theme.breakpoints.down("sm")]: {
      marginLeft: "5%",
      marginRight: "5%"
    }
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary
  },
  label: {
    fontWeight: "bold"
  },
  inputField: {
    marginBottom: "20px",
    backgroundColor: "#F7F7F7"
  },
  para: {
    color: "red"
  },
  flexDiv: {
    marginBottom: "10px",
    display: "flex"
  },
  logoTitle: {
    color: "grey",
    textAlign: "center"
  },
  logoImg: {
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    width: "120px",
    height: "50px"
  },
  chooseButton: {
    borderRadius: "5px",
    marginRight: "20px",
    width: "100px",
    height: "40px",
    color: "#fff",
    border: "1px solid #00BBDC",
    backgroundColor: "#00BBDC"
  },

  doneIcon: {
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    backgroundColor: "#00BBDC",
    color: "#fff",
    borderRadius: "50%",
    fontSize: "100px"
  },
  doneIcontitle: {
    textAlign: "center",
    color: "#00BBDC"
  },
  doneIconPara: {
    marginTop: "-10px",
    fontSize: "13px",
    textAlign: "center",
    color: "grey"
  },
  issueButton: {
    marginTop: "100px",
    textTransform: "none",
    fontSize: "20px",
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    color: "#fff",
    backgroundColor: "#00BBDC",
    borderRadius: "20px",
    width: "70%",
    "&:hover": {
      background: "#fff",
      color: "#00BBDC",
      border: "2px solid #00BBDC"
    }
  }
}));
